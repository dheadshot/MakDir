MakDir
=========

(Compile with kernel32)

A better Windows MD command, designed as a replacement for Unix's `mkdir` tool.

Usage: makdir [/P] <directory name>
Where <directory name> is the directory to create and /P creates the whole path.

Copyright (c) 2021 DHeadshot's Software Creations
MIT Licence

