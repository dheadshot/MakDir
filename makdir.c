//Compile with kernel32
#include <windows.h>
//#include <fileapi.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <ctype.h>

int streq_(char *a, char *b)
{
  unsigned long la, lb;
  la = strlen(a);
  lb = strlen(b);
  if (la != lb) return 0;
  if (memcmp(a,b,la)==0) return 1; else return 0;
}

int main (int argc, char *argv[])
{
    DWORD dwError;
    char *apath = NULL;
    int n=-1;
    if ((argc < 2) || (streq_(argv[1],"/?")))
    {
        printf("\nDHSC makdir 0.01.01\nCopyright (c) 2021 DHeadshot's Software Creations\nMIT Licence\nTwitter @DHeadshot\nMastodon @dheadshot@mastodon.social\n");
        printf("A Better Windows MD\n");
        printf("Usage: %s [/P] <directory name>\nWhere <directory name> is the directory to create and /P creates the whole path.\n", argv[0]);
        return 0;
    }
    if ((streq_(argv[1],"/P") || streq_(argv[1],"/p") ) && argc > 2)
    {
#ifdef DEBUG
        fprintf(stderr,"Creating path '%s'.\n",argv[2]);
#endif
        apath = (char *) malloc(sizeof(char)*(1+strlen(argv[2])));
        if (!apath)
        {
            fprintf(stderr,"Error: Out of Memory!");
            return 1;
        }
        do
        {
            if (n>=0 && argv[2][n]==0) break;
            n++;
            strcpy(apath,argv[2]);
            while (apath[n]!=0 && apath[n]!='\\' && apath[n]!='/') n++;
            if ((apath[n]=='\\' || apath[n]=='/') && apath[n+1]==0)
            {
                apath[n] = 0;
                n++;
            }
            else apath[n] = 0;
#ifdef DEBUG
            fprintf(stderr,"Creating directory '%s'.\n",apath);
#endif
            if (!CreateDirectory(apath,NULL)) dwError = GetLastError();
            else dwError = 0;
        } while (dwError == 0 || dwError == ERROR_ALREADY_EXISTS || argv[2][n]==0);
        
        free(apath);
        if (dwError == ERROR_ALREADY_EXISTS) dwError = 0;//fprintf(stderr,"Error: Path \"%s\" Already Exists!",argv[2]);
        else if (dwError == ERROR_PATH_NOT_FOUND) fprintf(stderr,"Error: Path \"%s\" Not Found!",argv[2]);
        else if (dwError != ERROR_ALREADY_EXISTS && dwError) fprintf(stderr,"Error: Unknown Error 0x%X!",dwError);
        return dwError;
    }
    else if (!CreateDirectory(argv[1],NULL))
    {
        dwError = GetLastError();
        if (dwError == ERROR_ALREADY_EXISTS) fprintf(stderr,"Error: Directory \"%s\" Already Exists!",argv[1]);
        else if (dwError == ERROR_PATH_NOT_FOUND) fprintf(stderr,"Error: Path \"%s\" Not Found!",argv[1]);
        else fprintf(stderr,"Error: Unknown Error 0x%X!",dwError);
        return dwError;
    }
    return 0;
}

